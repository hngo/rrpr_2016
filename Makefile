all:
	make pdf

pdf:
	pdflatex RRPR_review

clean:
	rm -f RRPR_review.pdf RRPR_review.log RRPR_review.bbl RRPR_review.blg RRPR_review.aux *~